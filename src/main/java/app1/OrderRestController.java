package app1;

import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class OrderRestController {

    @RequestMapping("/orders")
    public List<Order> getOrders() {
        Order order1 = new Order("111-222-333", "Apple");
        Order order2 = new Order("444-555-666", "JD.com");
        return Arrays.asList(order1, order2);
    }

    @RequestMapping("/order")
    public Order getOrder() {
        return new Order("111-222-333", "Apple");
    }
}
package app1;

public class Order {
    private String orderId;
    private String shipBy;

    public Order(String orderId, String shipBy) {
        this.orderId = orderId;
        this.shipBy = shipBy;
    }

    /**
     * @return the orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * @param orderId the orderId to set
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * @return the shipBy
     */
    public String getShipBy() {
        return shipBy;
    }

    /**
     * @param shipBy the shipBy to set
     */
    public void setShipBy(String shipBy) {
        this.shipBy = shipBy;
    }

}
# Getting started

## Step-1: Start Keycloak server on 8180 port
```bash
# Start Keycloak server
cd /Users/chunywan/work/keycloak-apps/keycloak-4.0.0.Beta1/bin
./standalone.sh -Djboss.socket.binding.port-offset=100

# http://localhost:8180/auth/
# admin/password
```

## Step-2: Setup realm, client and user from http://localhost:8180/auth/

- realm: demo
- clients: app1 (http://localhost:8080) and app2 (http://localhost:8081)
- user: user1/password
- role: create role named "user" and assign to user1 above. 

## Step-3: Start App1 & App2
```bash
cd /Users/chunywan/code/bitbucket/sso/oidc-app1
./gradlew bootRun
```

## Step-4: access app1 and app2
http://localhost:8080/index.html
http://localhost:8081/index.html
